Data Analytics with HPC: Linear Regression Practical
====================================================

In this practical you will learn to execute simple linear regressions using one
and them multiple variables. You will also look at polynomial regression.

