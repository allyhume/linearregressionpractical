library(shiny)
library(scatterplot3d) 


# Define server logic required to draw a histogram
shinyServer(function(input, output) {
  
  # Expression that generates a histogram. The expression is
  # wrapped in a call to renderPlot to indicate that:
  #
  #  1) It is "reactive" and therefore should re-execute automatically
  #     when inputs change
  #  2) Its output type is a plot
  
  output$distPlot <- renderPlot({

    s3d <- scatterplot3d(mtcars$wt,mtcars$disp,mtcars$mpg, main="3D Scatterplot")
        
    s3d$plane3d(c(input$c, input$a, input$b))

    # If fitting the correct plane
    # fit <- lm(mtcars$mpg ~ mtcars$wt+mtcars$disp) 
    # s3d$plane3d(fit)
    
    # Calculate sum of errors
    predicted <- input$a*mtcars$wt + input$b*mtcars$disp + input$c
    sumOfSquares <- sum((predicted-mtcars$mpg)^2)
    
    output$text1 <- renderText({ 
      paste("h(x) = ", input$a, "x + ", input$b, "  cost is ", sumOfSquares)
    })
    
  })
})
